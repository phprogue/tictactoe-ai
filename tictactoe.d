/*
 * Console Tic Tac Toe
 * with neural network ai
 * Uses minimax algorithm to calculate best move
 */

import std.stdio;
import std.string;
import std.conv;
import std.random;
import std.algorithm.comparison;

// X is human player
// O is computer player

enum N = 3;
enum WINNER_X = 0;
enum WINNER_O = 1;
enum GAME_RUNNING = 2;
enum GAME_OVER_TIE = -1;

struct Game {
    char[N * N] board;
}
int player_turn;
long computer_evals = 0;

char[] player_chars = ['X', 'O'];
bool game_over = false;

void main()
{
    Game game;
    game.board = ' '; // set all squares to blank
    player_turn = 1;

    auto rnd = Random(unpredictableSeed);

    render_board(game);

    int mov;
    while (!game_over) {
        if (player_turn == 0) {
            // human player
            mov = ask_for_move(game);
        } else {
            // computer move
            writeln("Computer move...");
            mov = computer_move(game);
        }
        if (mov != 9)
            make_move(game, mov);

        render_board(game);

        int current_state = check_game_over(game);
        if (current_state == WINNER_O || current_state == WINNER_X) {
            writeln("WINNER - ", player_chars[player_turn]);
            game_over = true;
        } else if (current_state == GAME_OVER_TIE) {
            writeln("Game Over - TIE");
            game_over = true;
        } else {
            player_turn++;
            if (player_turn == 2)
                player_turn = 0;
        }
    }
}

int ask_for_move(ref Game game)
{
    while (1) {
        write("Select a square to move: ");
        string m = chop(readln());
        if (m.length > 0) {
            int i = to!int(m);
            if (i >= 0 && i <= 8 && game.board[i] == ' ') {
                return i;
            }
        }
    }
}

int computer_move(ref Game game)
{
    int i = minimax(game, 0, -int.max, int.max, true);
    writefln("Computer looked at %s moves.", computer_evals);
    writeln("Best score = ", i);
    computer_evals = 0;

    // Select random squares for the computer move
    //while (1) {
    //    int i = uniform(0, 8);
    //    if (game.board[i] == ' ')
    //        return i;
    //}

    return i;
}

void make_move(ref Game game, int square)
{
    if (game.board[square] == ' ') {
        game.board[square] = player_chars[player_turn];
    }
}

int check_game_over(ref Game game)
{
    // check diagonals
    if (game.board[0] != ' ' && game.board[0] == game.board[4] && game.board[4] == game.board[8]) {
        return game.board[0] == 'X' ? 0 : 1;
    }
    if (game.board[2] != ' ' && game.board[2] == game.board[4] && game.board[4] == game.board[6]) {
        return game.board[2] == 'X' ? 0 : 1;
    }

    // check rows
    for (int r = 0; r < 3; r++) {
        if (game.board[r * 3] != ' ' && game.board[r * 3] == game.board[r * 3 + 1] && game.board[r * 3 + 1] == game.board[r * 3 + 2])
            return game.board[r * 3] == 'X' ? 0 : 1;
    }

    // check columns
    for (int c = 0; c < 3; c++) {
        if (game.board[c] != ' ' && game.board[c] == game.board[c + 3] && game.board[c + 3] == game.board[c + 6])
            return game.board[c] == 'X' ? 0 : 1;
    }

    // check tie
    foreach (s; game.board) {
        if (s == ' ') {
            return GAME_RUNNING;
        }
    }

    return GAME_OVER_TIE;
}

void render_board(ref Game game)
{
    write("\033[32;1m"); // color green
    writefln(" %c | %c | %c", game.board[0], game.board[1], game.board[2]);
    writeln("-----------");
    writefln(" %c | %c | %c", game.board[3], game.board[4], game.board[5]);
    writeln("-----------");
    writefln(" %c | %c | %c", game.board[6], game.board[7], game.board[8]);
    writeln();
    write("\033[0m"); // reset color
}

int minimax(ref Game game, int depth, int alpha, int beta, bool is_maximizer)
{
    int best_position;
    int winner = check_game_over(game);
    if (winner != GAME_RUNNING) {
        if (winner == WINNER_X) {
            return -10 + depth;
        } else if (winner == WINNER_O) {
            return 10 - depth;
        } else {
            return 0;
        }
    }

    computer_evals++;

    if (is_maximizer) {
        int max_eval = -int.max;
        int empty_squares = count_empty_squares(game);

        for (int i = 0; i < empty_squares; i++) {
            Game game_copy = game;
            int position = fill_empty_square(i, 'O', game_copy);
            int eval = minimax(game_copy, depth + 1, alpha, beta, false);

            if (eval > max_eval) {
                max_eval = eval;
                best_position = position;
            }
            alpha = max(alpha, eval);
            if (beta <= alpha) {
                break;
            }
        }
        if (depth == 0) {
            return best_position;
        }
        return max_eval;
    } else {
        int min_eval = int.max;
        int empty_squares = count_empty_squares(game);

        for (int i = 0; i < empty_squares; i++) {
            Game game_copy = game;
            int position = fill_empty_square(i, 'X', game_copy);
            int eval = minimax(game_copy, depth + 1, alpha, beta, true);
            min_eval = min(eval, min_eval);
            beta = min(eval, beta);
            if (beta <= alpha) {
                break;
            }
        }
        return min_eval;
    }

    return 0;
}

int count_empty_squares(ref Game game)
{
    int c = 0;
    foreach (sq; game.board) {
        if (sq == ' ')
            c++;
    }
    return c;
}

int fill_empty_square(int index, char c, ref Game game_copy) {
    int empty_index = 0;
    foreach (int position, s; game_copy.board) {
        if (game_copy.board[position] == ' ') {
            if (index == empty_index) {
                game_copy.board[position] = c;
                return position;
            }
            empty_index++;
        }
    }
    return -1;
}

